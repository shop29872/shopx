/**
 * @fileoverview gRPC-Web generated client stub for ifnoshop
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.ifnoshop = require('./shop_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.ifnoshop.ShopServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.ifnoshop.ShopServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.SendSMSRequest,
 *   !proto.ifnoshop.SendSMSResponse>}
 */
const methodDescriptor_ShopService_SendSms = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/SendSms',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.SendSMSRequest,
  proto.ifnoshop.SendSMSResponse,
  /**
   * @param {!proto.ifnoshop.SendSMSRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.SendSMSResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.SendSMSRequest,
 *   !proto.ifnoshop.SendSMSResponse>}
 */
const methodInfo_ShopService_SendSms = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.SendSMSResponse,
  /**
   * @param {!proto.ifnoshop.SendSMSRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.SendSMSResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.SendSMSRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.SendSMSResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.SendSMSResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.sendSms =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/SendSms',
      request,
      metadata || {},
      methodDescriptor_ShopService_SendSms,
      callback);
};


/**
 * @param {!proto.ifnoshop.SendSMSRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.SendSMSResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.sendSms =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/SendSms',
      request,
      metadata || {},
      methodDescriptor_ShopService_SendSms);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.LoginRequest,
 *   !proto.ifnoshop.LoginResponse>}
 */
const methodDescriptor_ShopService_Login = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/Login',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.LoginRequest,
  proto.ifnoshop.LoginResponse,
  /**
   * @param {!proto.ifnoshop.LoginRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.LoginResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.LoginRequest,
 *   !proto.ifnoshop.LoginResponse>}
 */
const methodInfo_ShopService_Login = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.LoginResponse,
  /**
   * @param {!proto.ifnoshop.LoginRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.LoginResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.LoginRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.LoginResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.LoginResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.login =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/Login',
      request,
      metadata || {},
      methodDescriptor_ShopService_Login,
      callback);
};


/**
 * @param {!proto.ifnoshop.LoginRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.LoginResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.login =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/Login',
      request,
      metadata || {},
      methodDescriptor_ShopService_Login);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.UpdateProfileRequest,
 *   !proto.ifnoshop.UpdateProfileResponse>}
 */
const methodDescriptor_ShopService_UpdateProfile = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/UpdateProfile',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.UpdateProfileRequest,
  proto.ifnoshop.UpdateProfileResponse,
  /**
   * @param {!proto.ifnoshop.UpdateProfileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.UpdateProfileResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.UpdateProfileRequest,
 *   !proto.ifnoshop.UpdateProfileResponse>}
 */
const methodInfo_ShopService_UpdateProfile = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.UpdateProfileResponse,
  /**
   * @param {!proto.ifnoshop.UpdateProfileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.UpdateProfileResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.UpdateProfileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.UpdateProfileResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.UpdateProfileResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.updateProfile =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/UpdateProfile',
      request,
      metadata || {},
      methodDescriptor_ShopService_UpdateProfile,
      callback);
};


/**
 * @param {!proto.ifnoshop.UpdateProfileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.UpdateProfileResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.updateProfile =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/UpdateProfile',
      request,
      metadata || {},
      methodDescriptor_ShopService_UpdateProfile);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.AddCategoriesRequest,
 *   !proto.ifnoshop.AddCategoriesResponse>}
 */
const methodDescriptor_ShopService_AddCategories = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/AddCategories',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.AddCategoriesRequest,
  proto.ifnoshop.AddCategoriesResponse,
  /**
   * @param {!proto.ifnoshop.AddCategoriesRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.AddCategoriesResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.AddCategoriesRequest,
 *   !proto.ifnoshop.AddCategoriesResponse>}
 */
const methodInfo_ShopService_AddCategories = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.AddCategoriesResponse,
  /**
   * @param {!proto.ifnoshop.AddCategoriesRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.AddCategoriesResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.AddCategoriesRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.AddCategoriesResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.AddCategoriesResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.addCategories =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/AddCategories',
      request,
      metadata || {},
      methodDescriptor_ShopService_AddCategories,
      callback);
};


/**
 * @param {!proto.ifnoshop.AddCategoriesRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.AddCategoriesResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.addCategories =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/AddCategories',
      request,
      metadata || {},
      methodDescriptor_ShopService_AddCategories);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.AddBrandRequest,
 *   !proto.ifnoshop.AddBrandResponse>}
 */
const methodDescriptor_ShopService_AddBrand = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/AddBrand',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.AddBrandRequest,
  proto.ifnoshop.AddBrandResponse,
  /**
   * @param {!proto.ifnoshop.AddBrandRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.AddBrandResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.AddBrandRequest,
 *   !proto.ifnoshop.AddBrandResponse>}
 */
const methodInfo_ShopService_AddBrand = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.AddBrandResponse,
  /**
   * @param {!proto.ifnoshop.AddBrandRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.AddBrandResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.AddBrandRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.AddBrandResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.AddBrandResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.addBrand =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/AddBrand',
      request,
      metadata || {},
      methodDescriptor_ShopService_AddBrand,
      callback);
};


/**
 * @param {!proto.ifnoshop.AddBrandRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.AddBrandResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.addBrand =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/AddBrand',
      request,
      metadata || {},
      methodDescriptor_ShopService_AddBrand);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.AddTagRequest,
 *   !proto.ifnoshop.AddTagResponse>}
 */
const methodDescriptor_ShopService_AddTag = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/AddTag',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.AddTagRequest,
  proto.ifnoshop.AddTagResponse,
  /**
   * @param {!proto.ifnoshop.AddTagRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.AddTagResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.AddTagRequest,
 *   !proto.ifnoshop.AddTagResponse>}
 */
const methodInfo_ShopService_AddTag = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.AddTagResponse,
  /**
   * @param {!proto.ifnoshop.AddTagRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.AddTagResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.AddTagRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.AddTagResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.AddTagResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.addTag =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/AddTag',
      request,
      metadata || {},
      methodDescriptor_ShopService_AddTag,
      callback);
};


/**
 * @param {!proto.ifnoshop.AddTagRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.AddTagResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.addTag =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/AddTag',
      request,
      metadata || {},
      methodDescriptor_ShopService_AddTag);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.AddProductRequest,
 *   !proto.ifnoshop.AddProductResponse>}
 */
const methodDescriptor_ShopService_AddProduct = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/AddProduct',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.AddProductRequest,
  proto.ifnoshop.AddProductResponse,
  /**
   * @param {!proto.ifnoshop.AddProductRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.AddProductResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.AddProductRequest,
 *   !proto.ifnoshop.AddProductResponse>}
 */
const methodInfo_ShopService_AddProduct = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.AddProductResponse,
  /**
   * @param {!proto.ifnoshop.AddProductRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.AddProductResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.AddProductRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.AddProductResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.AddProductResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.addProduct =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/AddProduct',
      request,
      metadata || {},
      methodDescriptor_ShopService_AddProduct,
      callback);
};


/**
 * @param {!proto.ifnoshop.AddProductRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.AddProductResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.addProduct =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/AddProduct',
      request,
      metadata || {},
      methodDescriptor_ShopService_AddProduct);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.AddProfileAddressRequest,
 *   !proto.ifnoshop.AddProfileAddressResponse>}
 */
const methodDescriptor_ShopService_AddProfileAddress = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/AddProfileAddress',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.AddProfileAddressRequest,
  proto.ifnoshop.AddProfileAddressResponse,
  /**
   * @param {!proto.ifnoshop.AddProfileAddressRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.AddProfileAddressResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.AddProfileAddressRequest,
 *   !proto.ifnoshop.AddProfileAddressResponse>}
 */
const methodInfo_ShopService_AddProfileAddress = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.AddProfileAddressResponse,
  /**
   * @param {!proto.ifnoshop.AddProfileAddressRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.AddProfileAddressResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.AddProfileAddressRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.AddProfileAddressResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.AddProfileAddressResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.addProfileAddress =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/AddProfileAddress',
      request,
      metadata || {},
      methodDescriptor_ShopService_AddProfileAddress,
      callback);
};


/**
 * @param {!proto.ifnoshop.AddProfileAddressRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.AddProfileAddressResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.addProfileAddress =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/AddProfileAddress',
      request,
      metadata || {},
      methodDescriptor_ShopService_AddProfileAddress);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.RemoveProfileAddressRequest,
 *   !proto.ifnoshop.RemoveProfileAddressResponse>}
 */
const methodDescriptor_ShopService_RemoveProfileAddress = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/RemoveProfileAddress',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.RemoveProfileAddressRequest,
  proto.ifnoshop.RemoveProfileAddressResponse,
  /**
   * @param {!proto.ifnoshop.RemoveProfileAddressRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.RemoveProfileAddressResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.RemoveProfileAddressRequest,
 *   !proto.ifnoshop.RemoveProfileAddressResponse>}
 */
const methodInfo_ShopService_RemoveProfileAddress = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.RemoveProfileAddressResponse,
  /**
   * @param {!proto.ifnoshop.RemoveProfileAddressRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.RemoveProfileAddressResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.RemoveProfileAddressRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.RemoveProfileAddressResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.RemoveProfileAddressResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.removeProfileAddress =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/RemoveProfileAddress',
      request,
      metadata || {},
      methodDescriptor_ShopService_RemoveProfileAddress,
      callback);
};


/**
 * @param {!proto.ifnoshop.RemoveProfileAddressRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.RemoveProfileAddressResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.removeProfileAddress =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/RemoveProfileAddress',
      request,
      metadata || {},
      methodDescriptor_ShopService_RemoveProfileAddress);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.UpdateCardItemRequest,
 *   !proto.ifnoshop.UpdateCardItemResponse>}
 */
const methodDescriptor_ShopService_UpdateCardItem = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/UpdateCardItem',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.UpdateCardItemRequest,
  proto.ifnoshop.UpdateCardItemResponse,
  /**
   * @param {!proto.ifnoshop.UpdateCardItemRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.UpdateCardItemResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.UpdateCardItemRequest,
 *   !proto.ifnoshop.UpdateCardItemResponse>}
 */
const methodInfo_ShopService_UpdateCardItem = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.UpdateCardItemResponse,
  /**
   * @param {!proto.ifnoshop.UpdateCardItemRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.UpdateCardItemResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.UpdateCardItemRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.UpdateCardItemResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.UpdateCardItemResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.updateCardItem =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/UpdateCardItem',
      request,
      metadata || {},
      methodDescriptor_ShopService_UpdateCardItem,
      callback);
};


/**
 * @param {!proto.ifnoshop.UpdateCardItemRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.UpdateCardItemResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.updateCardItem =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/UpdateCardItem',
      request,
      metadata || {},
      methodDescriptor_ShopService_UpdateCardItem);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.RemoveCardRequest,
 *   !proto.ifnoshop.RemoveCardResponse>}
 */
const methodDescriptor_ShopService_RemoveCardItem = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/RemoveCardItem',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.RemoveCardRequest,
  proto.ifnoshop.RemoveCardResponse,
  /**
   * @param {!proto.ifnoshop.RemoveCardRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.RemoveCardResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.RemoveCardRequest,
 *   !proto.ifnoshop.RemoveCardResponse>}
 */
const methodInfo_ShopService_RemoveCardItem = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.RemoveCardResponse,
  /**
   * @param {!proto.ifnoshop.RemoveCardRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.RemoveCardResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.RemoveCardRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.RemoveCardResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.RemoveCardResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.removeCardItem =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/RemoveCardItem',
      request,
      metadata || {},
      methodDescriptor_ShopService_RemoveCardItem,
      callback);
};


/**
 * @param {!proto.ifnoshop.RemoveCardRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.RemoveCardResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.removeCardItem =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/RemoveCardItem',
      request,
      metadata || {},
      methodDescriptor_ShopService_RemoveCardItem);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.GetUserCardRequest,
 *   !proto.ifnoshop.GetUserCardResponse>}
 */
const methodDescriptor_ShopService_GetUserCard = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/GetUserCard',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.GetUserCardRequest,
  proto.ifnoshop.GetUserCardResponse,
  /**
   * @param {!proto.ifnoshop.GetUserCardRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.GetUserCardResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.GetUserCardRequest,
 *   !proto.ifnoshop.GetUserCardResponse>}
 */
const methodInfo_ShopService_GetUserCard = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.GetUserCardResponse,
  /**
   * @param {!proto.ifnoshop.GetUserCardRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.GetUserCardResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.GetUserCardRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.GetUserCardResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.GetUserCardResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.getUserCard =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/GetUserCard',
      request,
      metadata || {},
      methodDescriptor_ShopService_GetUserCard,
      callback);
};


/**
 * @param {!proto.ifnoshop.GetUserCardRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.GetUserCardResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.getUserCard =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/GetUserCard',
      request,
      metadata || {},
      methodDescriptor_ShopService_GetUserCard);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.CreateInvoiceRequest,
 *   !proto.ifnoshop.CreateInvoiceResponse>}
 */
const methodDescriptor_ShopService_CreateInvoice = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/CreateInvoice',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.CreateInvoiceRequest,
  proto.ifnoshop.CreateInvoiceResponse,
  /**
   * @param {!proto.ifnoshop.CreateInvoiceRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.CreateInvoiceResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.CreateInvoiceRequest,
 *   !proto.ifnoshop.CreateInvoiceResponse>}
 */
const methodInfo_ShopService_CreateInvoice = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.CreateInvoiceResponse,
  /**
   * @param {!proto.ifnoshop.CreateInvoiceRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.CreateInvoiceResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.CreateInvoiceRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.CreateInvoiceResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.CreateInvoiceResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.createInvoice =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/CreateInvoice',
      request,
      metadata || {},
      methodDescriptor_ShopService_CreateInvoice,
      callback);
};


/**
 * @param {!proto.ifnoshop.CreateInvoiceRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.CreateInvoiceResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.createInvoice =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/CreateInvoice',
      request,
      metadata || {},
      methodDescriptor_ShopService_CreateInvoice);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.RevertInvoiceRequest,
 *   !proto.ifnoshop.RevertInvoiceResponse>}
 */
const methodDescriptor_ShopService_RevertInvoice = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/RevertInvoice',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.RevertInvoiceRequest,
  proto.ifnoshop.RevertInvoiceResponse,
  /**
   * @param {!proto.ifnoshop.RevertInvoiceRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.RevertInvoiceResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.RevertInvoiceRequest,
 *   !proto.ifnoshop.RevertInvoiceResponse>}
 */
const methodInfo_ShopService_RevertInvoice = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.RevertInvoiceResponse,
  /**
   * @param {!proto.ifnoshop.RevertInvoiceRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.RevertInvoiceResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.RevertInvoiceRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.RevertInvoiceResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.RevertInvoiceResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.revertInvoice =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/RevertInvoice',
      request,
      metadata || {},
      methodDescriptor_ShopService_RevertInvoice,
      callback);
};


/**
 * @param {!proto.ifnoshop.RevertInvoiceRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.RevertInvoiceResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.revertInvoice =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/RevertInvoice',
      request,
      metadata || {},
      methodDescriptor_ShopService_RevertInvoice);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ifnoshop.SearchProductRequest,
 *   !proto.ifnoshop.SearchProductResponse>}
 */
const methodDescriptor_ShopService_SearchProduct = new grpc.web.MethodDescriptor(
  '/ifnoshop.ShopService/SearchProduct',
  grpc.web.MethodType.UNARY,
  proto.ifnoshop.SearchProductRequest,
  proto.ifnoshop.SearchProductResponse,
  /**
   * @param {!proto.ifnoshop.SearchProductRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.SearchProductResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ifnoshop.SearchProductRequest,
 *   !proto.ifnoshop.SearchProductResponse>}
 */
const methodInfo_ShopService_SearchProduct = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ifnoshop.SearchProductResponse,
  /**
   * @param {!proto.ifnoshop.SearchProductRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ifnoshop.SearchProductResponse.deserializeBinary
);


/**
 * @param {!proto.ifnoshop.SearchProductRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ifnoshop.SearchProductResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ifnoshop.SearchProductResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ifnoshop.ShopServiceClient.prototype.searchProduct =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ifnoshop.ShopService/SearchProduct',
      request,
      metadata || {},
      methodDescriptor_ShopService_SearchProduct,
      callback);
};


/**
 * @param {!proto.ifnoshop.SearchProductRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ifnoshop.SearchProductResponse>}
 *     Promise that resolves to the response
 */
proto.ifnoshop.ShopServicePromiseClient.prototype.searchProduct =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/ifnoshop.ShopService/SearchProduct',
      request,
      metadata || {},
      methodDescriptor_ShopService_SearchProduct);
};


module.exports = proto.ifnoshop;

